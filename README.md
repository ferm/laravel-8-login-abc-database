# linuxitos 

![Login.](public/images/1.png)

## Entorno de desarrollo
- PHP > 7.4
- Fedora 33 x86_64 (no probado con otros SO)
- MariaDB 10.4
- Bootstrap 4.5.3
- XAMPP 8.0.0

## Instalación
- Previamente instalado XAMPP o PHP
- Montar la bd con el script
- Modificar el archivo .env para la url del proyecto y la conexión a la bd

## ¿Qué incluye?
- Script base de datos
- Todos los archivos del proyecto

## ¿Funciones del proyecto?
- Login
- Registro
- Modificar perfil
- Modificar contraseña
- Añadir, eliminar, editar registros

## 1 ¿Qué es laravel?
Laravel es uno de los frameworks de código abierto más fáciles de asimilar para PHP. Es simple, muy potente y tiene una interfaz elegante y divertida de usar. Fue creado en 2011 y tiene una gran influencia de frameworks como Ruby on Rails, Sinatra y ASP.NET MVC, Codeigniter, entre otros.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## 2 ¿Qué es XAMPP?

XAMPP es una distribución de Apache completamente gratuita y fácil de instalar que contiene MariaDB, PHP y Perl. El paquete de instalación de XAMPP ha sido diseñado para ser increíblemente fácil de instalar y usar.

Es la mejor opción para iniciarse en el desarrollo web con php, bases de datos, etc, en su lugar pueden utilizar otras opción, al final de cuentas, sólo es eso, opciones.

El proceso de instalación lo realicé por separado, así que para que lo puedan revisar, seguir el siguiente post. https://blog.linuxitos.com/instalar-xampp-8-0-0-fedora-33

![Login.](public/images/2.png)
![Login.](public/images/3.png)
![Login.](public/images/4.png)
![Login.](public/images/5.png)
![Login.](public/images/6.png)
![Login.](public/images/7.png)