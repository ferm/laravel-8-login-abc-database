@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title text-center mb-4">Confirmar contraseña</h5>
					<div class="row">
						<div class="col-md-12">
							<p class="">
								Antes de continuar, por favor confirma tu contraseña
							</p>
						</div>
					</div>

					<form method="POST" action="{{ route('password.confirm') }}">
						@csrf
						<div class="row">
							<div class="col-md-12">
								<div class="form-group input-group">
									<span class="has-float-label">
										<input id="password" type="password" placeholder=" " class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
										@error('password')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
										<label for="password">Contraseña</label>
										<i id="icon-eye" class="fa fa-eye-slash form-control-feedback btn-show-passwd" data-passwd="password"></i>
									</span>
								</div>
							</div>
							
							<div class="col-md-12">
								<a class="text-muted" href="{{ route('password.request') }}" id="a-cnt-forget-login">
									<i class="fa fa-chevron-left"></i> ¿Olvidaste contraseña?
								</a>
								<button type="submit" class="btn btn-primary float-right" id="btn-recover-passwd" name="btn-recover-passwd">
									<i class="fa fa-check"></i> Continuar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
