@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title text-center mb-4">¿Olvidaste la contraseña?</h5>
					<div class="row">
						<div class="col-md-12">
							<p class="">
								Ingresa tu correo electrónico y se te enviará una contraseña temporal para reestablecer tu acceso.
							</p>
						</div>
					</div>

					@if (session('status'))
						<div class="alert alert-success" role="alert">
							{{ session('status') }}
						</div>
					@endif

					<form method="POST" action="{{ route('password.email') }}">
						@csrf
						<div class="row">
							<div class="col-md-12">
								<div class="form-group input-group">
									<span class="has-float-label">
										<input id="email" type="email" placeholder=" " class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required="" autocomplete="email" autofocus="">
										@error('email')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
										<label for="email">Email</label>
										<i class="fa fa-at form-control-feedback"></i>
									</span>
								</div>
							</div>
						
							<div class="col-md-12">
								<a class="text-muted" href="{{url('login')}}" id="a-cnt-forget-login">
									<i class="fa fa-chevron-left"></i> Iniciar sesión
								</a>

								<button type="submit" class="btn btn-primary float-right" id="btn-recover-passwd" name="btn-recover-passwd">
									<i class="fa fa-check"></i> Continuar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
