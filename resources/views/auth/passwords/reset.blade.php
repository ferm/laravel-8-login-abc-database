@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title text-center mb-4">Confirmar contraseña</h5>
					<div class="row">
						<div class="col-md-12">
							<p class="">
								Antes de continuar, por favor confirma tu contraseña
							</p>
						</div>
					</div>
					<form method="POST" action="{{ route('password.update') }}">
						@csrf
						<input type="hidden" name="token" value="{{ $token }}">
						<div class="row">
							<div class="col-12">
								<div class="form-group input-group">
									<span class="has-float-label">
										<input id="email" type="email" class="form-control float-form @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required="" autocomplete="email">
										@error('email')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
										<label for="email">Email</label>
										<i class="fa fa-at form-control-feedback"></i>
									</span>
								</div>
							</div>

							<div class="col-12 col-sm-6">
								<div class="form-group input-group">
									<span class="has-float-label">
										<input id="password" type="password" class="form-control float-form @error('password') is-invalid @enderror" name="password" required="" autocomplete="new-password">
										@error('password')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
										<label for="password">Contraseña</label>
										<i id="icon-eye" class="fa fa-eye-slash form-control-feedback btn-show-passwd" data-passwd="password"></i>
									</span>
								</div>
							 </div>

							 <div class="col-12 col-sm-6">
								<div class="form-group input-group">
									<span class="has-float-label">
										<input id="password-confirm" type="password" class="form-control float-form matches-passwd" name="password_confirmation" required autocomplete="new-password" data-passwd="password" data-confirm="password-confirm">
										<label for="password-confirm">Confirmar contraseña</label>
										<i class="fa fa-eye-slash form-control-feedback btn-show-passwd" data-passwd="password-confirm"></i>
									</span>
								</div>
							</div>
						
							<div class="col-md-12">
								<a class="text-muted" href="{{ url('login') }}" id="a-cnt-forget-login">
									<i class="fa fa-chevron-left"></i> Iniciar sesión
								</a>
								<button type="submit" class="btn btn-primary float-right" id="btn-recover-passwd" name="btn-recover-passwd">
									<i class="fa fa-check"></i> Continuar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
