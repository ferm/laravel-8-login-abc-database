@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<form method="post" enctype="multipart/form-data" id="form-login-user" name="form-login-user" accept-charset="utf-8" action="{{route('login')}}">
				@csrf
				<div class="card">
					<div class="card-body">
						<h5 class="text-center card-title mb-4">
							Inicio de Sesión
						</h5>
						<div id="div-cnt-login" class="row">
							<div class="col-12" id="div-cnt-msg-login"></div>
							<div class="col-md-12">
								<div class="form-group input-group">
									<span class="has-float-label">
										<input type="email" class="form-control float-form @error('email') is-invalid @enderror" placeholder=" " required="required" autocomplete="off" id="email" name="email" value="{{old('email')}}" />
										@error('email')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
										<label for="email">Usuario</label>
										<i class="fa fa-user form-control-feedback"></i>
									</span>
								</div>
							</div>
							
							<div class="col-md-12">
								<div class="form-group input-group">
									<span class="has-float-label">
										<input type="password" placeholder=" " class="form-control float-form @error('password') is-invalid @enderror" id="password" name="password" size="30" autocomplete="off">
										<label for="password">Contraseña</label>
										<i id="icon-eye" class="fa fa-eye-slash form-control-feedback btn-show-passwd" data-passwd="password"></i>
									</span>
								</div>
							</div>
							<div class="col-6">
								<a class="align-middle text-muted" href="{{route('password.request')}}" id="a-to-recover-passwd">
									Recupear contraseña? <i class="fa fa-chevron-right"></i>
								</a>
							</div>

							<div class="col-3 text-right mt-1">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
									<label class="custom-control-label" for="remember">Recordarme</label>
								</div>
							</div>
							<div class="col-3">
								<button type="submit" class="btn btn-primary btn-block" name="btn-login" id="btn-login">
									<i class="fa fa-sign-in"></i> Iniciar sesión
								</button>
							</div>
							<div class="col-md-12 mt-3 mb-2 text-center">
								<a class="" href="{{url('register')}}"><i class="fa fa-plus"></i> Registráte</a>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
