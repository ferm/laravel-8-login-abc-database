@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title text-center mb-4">Registro</h5>
					<form method="POST" action="{{ route('register') }}">
						@csrf

						<div class="form-row">
							<div class="col-12 col-sm-12">
								<div class="form-group input-group">
									<span class="has-float-label">
										<input id="name" type="text" class="form-control float-form @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required="" autofocus="">
										@error('name')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
										<label for="name">Nombre</label>
										<i class="fa fa-user form-control-feedback"></i>
									</span>
								</div>
							</div>

							<div class="col-12">
								<div class="form-group input-group">
									<span class="has-float-label">
										<input id="email" type="email" class="form-control float-form @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required="" autocomplete="email">
										@error('email')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
										<label for="email">Email</label>
										<i class="fa fa-at form-control-feedback"></i>
									</span>
								</div>
							</div>

							<div class="col-12 col-sm-6">
								<div class="form-group input-group">
									<span class="has-float-label">
										<input id="password" type="password" class="form-control float-form @error('password') is-invalid @enderror" name="password" required="" autocomplete="new-password">
										@error('password')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
										<label for="password">Contraseña</label>
										<i id="icon-eye" class="fa fa-eye-slash form-control-feedback btn-show-passwd" data-passwd="password"></i>
									</span>
								</div>
							 </div>

							 <div class="col-12 col-sm-6">
								<div class="form-group input-group">
									<span class="has-float-label">
										<input id="password-confirm" type="password" class="form-control float-form matches-passwd" name="password_confirmation" required autocomplete="new-password" data-passwd="password" data-confirm="password-confirm">
										<label for="password-confirm">Confirmar contraseña</label>
										<i class="fa fa-eye-slash form-control-feedback btn-show-passwd" data-passwd="password-confirm"></i>
									</span>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-12 col-md-12 col-12">
								<a class="float-start" href="{{url('login')}}">
									<i class="fa fa-chevron-left"></i> Iniciar sesión
								</a>

								<button type="submit" class="btn btn-primary float-right" id="btn-singup">
									<i class="fa fa-check"></i> Continuar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{{asset('resources/js/ajxlogin.js')}}"></script>
@endsection