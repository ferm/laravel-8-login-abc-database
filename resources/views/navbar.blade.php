<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top p-1">
	<div class="container-xl">
		<a class="navbar-brand" href="{{ url('/') }}">
			<img src="{{asset('public/images/logo.svg')}}" id="logo_custom"  alt="logo" height="20">
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarsExample07XL">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link" href="#">About</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Services</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Contact</a>
				</li>
				<!-- Authentication Links -->
				@guest
					<li class="nav-item">
						{!!(request()->is('login')) ? '' : '<a class="btn btn-outline-info btn-sm mt-1" href="'.route('login').'">Iniciar sesión</a>'!!}
					</li>
					@if (Route::has('register'))
						<li class="nav-item">
							{!!(request()->is('register')) ? '' : '<a class="btn btn-outline-success btn-sm mt-1 ml-1" href="'.route('register').'">Registrarse</a>'!!}
						</li>
					@endif
				@else
					<li class="nav-item dropdown desplegable">
						<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
							<span class="pull-left">
								<img src="{{(Auth::user()->image!='' && file_exists(public_path(Auth::user()->files.Auth::user()->image)) ? asset('public/'.Auth::user()->files.Auth::user()->image): asset('public/images/default.svg'))}}" class="img-circle rounded-circle bg-white" title="Imagen user" alt="Imagen user" width="30px" height="30px"> {{Auth::user()->name}}
							</span>
							<span class="caret"></span>
						</a>

						<div class="dropdown-menu dropdown-menu-right desplegable-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item {{request()->is('account')?'active':''}}" href="{{url('/account')}}">
								Perfil <i class="bi bi-person-circle float-right"></i>
							</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item {{request()->is('add')?'active':''}}" href="{{url('/add')}}">
								Agregar <i class="bi bi-plus float-right"></i>
							</a>
							<a class="dropdown-item {{(request()->is('posts') || request()->is('edit'))?'active':''}}" href="{{url('/posts')}}">
								Registros <i class="bi bi-list-ol float-right"></i>
							</a>

							<a class="dropdown-item" href="{{ route('logout') }}"
							   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
								Salir <i class="bi bi-power float-right"></i>
							</a>

							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								@csrf
							</form>
						</div>
					</li>
				@endguest
			</ul>
		</div>
	</div>
</nav>