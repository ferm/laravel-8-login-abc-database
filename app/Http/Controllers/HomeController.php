<?php

namespace App\Http\Controllers;
use App\Models\Posts;
use Illuminate\Http\Request;

class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(){
		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index(){
		return view('home');
	}

	public function view($slug){
		$post 		= Posts::where(['slug' => $slug])->first();
		if (!$post) {
			return redirect('todo')->with('error', 'Registro no encontrado, por favor revise.');
		}
		return view('posts.view', ['todo' => $post]);
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function adminHome(){
		return view('adminHome');
	}
}
