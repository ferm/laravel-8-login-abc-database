<?php
use App\Http\Controllers\PostsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', 'PostsController@main');
//Route::get('/', ['as' => 'main', 'uses' => 'PostsController@main']);
Route::get('/', [PostsController::class, 'main']);
Auth::routes();
/*Route::get('/', function () {
	return view('welcome');
});*/

/**
 * para el control y modificación de información de usuario*/
Route::group(['middleware' => 'auth'], function () {
	//Route::resource('/account', [UsersController::class, 'profile']);

	Route::resource('posts', PostsController::class);

	Route::post('/page', [PostsController::class, 'load']);
	Route::get('/page', ['as' => 'posts.post', 'uses' => [HomeController::class,'load']]);

	Route::get('/add', [PostsController::class, 'create']);
	Route::post('/postinsert', [PostsController::class, 'ajaxAdd']);
	Route::get('/account', [UsersController::class, 'index']);
	Route::post('/loadimage', [UsersController::class, 'loadImage']);
	Route::post('/accountupdate', [UsersController::class, 'upProfile']);
	Route::post('/upimage', [UsersController::class, 'upImage']);
	Route::post('/uppassword', [UsersController::class, 'upPassword']);

	Route::post('/postupdate', [PostsController::class, 'ajaxUpdate']);
	Route::post('/postdelete', [PostsController::class, 'ajaxDelete']);
});

/*Route::group(['middleware' => 'auth'], function () {
	Route::get('/post/{slug?}', ['as' => 'posts.post', 'uses' => [PostsController::class, 'view']]);
	Route::get('/add', [PostsController::class, 'create']);
	Route::get('/postinsert', [PostsController::class, 'ajaxAdd']);
	Route::post('/postinsert', [PostsController::class, 'ajaxAdd']);
	Route::get('/postupdate', [PostsController::class, 'ajaxUpdate']);
	
	
});*/

Route::get('/post/{slug}', [HomeController::class, 'view']);
//Route::get('/post/{slug}', [PostController::class, 'show']);
//Route::get('/post/{slug}', ['as' => 'posts.view', 'uses' => [HomeController::class,'view']]);

//Route::get('/{slug?}', ['as' => 'home.view', 'uses' => 'HomeController@view']);

//Auth::routes();

/*Route::get('/home', [App\Http\Controllers\HomeController::class, 'index']);


Route::get('admin/home', [HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');

Route::get('posts',['as'=>'posts.index','uses'=>'PostsController@index']);*/