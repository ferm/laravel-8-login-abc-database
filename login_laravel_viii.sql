-- phpMyAdmin SQL Dump
-- version 5.1.0-rc1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 18, 2021 at 08:34 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: 'login_laravel_viii'
--
CREATE DATABASE IF NOT EXISTS 'login_laravel_viii' DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE 'login_laravel_viii';

-- --------------------------------------------------------

--
-- Table structure for table 'failed_jobs'
--

DROP TABLE IF EXISTS 'failed_jobs';
CREATE TABLE IF NOT EXISTS 'failed_jobs' (
	'id' bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	'connection' text COLLATE utf8mb4_unicode_ci NOT NULL,
	'queue' text COLLATE utf8mb4_unicode_ci NOT NULL,
	'payload' longtext COLLATE utf8mb4_unicode_ci NOT NULL,
	'exception' longtext COLLATE utf8mb4_unicode_ci NOT NULL,
	'failed_at' timestamp NOT NULL DEFAULT current_timestamp(),
	PRIMARY KEY ('id')
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table 'migrations'
--

DROP TABLE IF EXISTS 'migrations';
CREATE TABLE IF NOT EXISTS 'migrations' (
	'id' int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	'migration' varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	'batch' int(11) NOT NULL,
	PRIMARY KEY ('id')
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table 'migrations'
--

INSERT INTO 'migrations' ('id', 'migration', 'batch') VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_06_15_140223_create_posts_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table 'password_resets'
--

DROP TABLE IF EXISTS 'password_resets';
CREATE TABLE IF NOT EXISTS 'password_resets' (
	'email' varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	'token' varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	'created_at' timestamp NULL DEFAULT NULL,
	KEY 'password_resets_email_index' ('email')
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table 'posts'
--

DROP TABLE IF EXISTS 'posts';
CREATE TABLE IF NOT EXISTS 'posts' (
	'id' bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	'title' varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	'desc' varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	'slug' varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	'files' varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	'image' varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	'status' enum('pendiente','publicado') COLLATE utf8mb4_unicode_ci NOT NULL,
	'user_id' int(11) NOT NULL,
	'created_at' timestamp NULL DEFAULT NULL,
	'updated_at' timestamp NULL DEFAULT NULL,
	PRIMARY KEY ('id')
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table 'users'
--

DROP TABLE IF EXISTS 'users';
CREATE TABLE IF NOT EXISTS 'users' (
	'id' bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	'name' varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	'email' varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	'email_verified_at' timestamp NULL DEFAULT NULL,
	'password' varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	'remember_token' varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
	'created_at' timestamp NULL DEFAULT NULL,
	'updated_at' timestamp NULL DEFAULT NULL,
	'files' varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
	'image' varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
	PRIMARY KEY ('id'),
	UNIQUE KEY 'users_email_unique' ('email')
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;